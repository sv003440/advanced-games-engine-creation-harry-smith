#pragma once
//3DS Max Object type details
#define MAX_VERTICES 15000
#define MAX_POLYGONS 15000

// 
struct Triangle
{
	int a;
	int b;
	int c;
};



enum class SCREENS
{
	INTRO = 0,
	NONE,
	MENU,
	LEVEL1,
	LEVEL2,
	GAMEOVER,
	HIGHSCORES,
	SPLASHSCREENLEVEL1,
	SPLASHSCREENLEVEL2,

};

struct Rect2D
{
	float x;
	float y;
	float width;
	float height;

	Rect2D(float initialX, float initialY, float initialWidth, float initialHeight)
	{
		x = initialX;
		y = initialY;
		width  = initialWidth;
		height = initialHeight;
	}
};

struct Vector2D
{
	float x;
	float y;

	Vector2D()
	{
		x = 0.0f;
		y = 0.0f;
	}

	Vector2D(float initialX, float initialY)
	{
		x = initialX;
		y = initialY;
	}
};

struct TexCoord
{
	float u;
	float v;

	TexCoord()
	{
		u = 0.0f;
		v = 0.0f;
	}

	TexCoord(float initialU, float initialV)
	{
		u = initialU;
		v = initialV;
	}
};

struct Vertex3D
{
	float x;
	float y;
	float z;
	//float padding? for byte alignment?
};

struct Colour
{
	float r;
	float g;
	float b;
	//float padding? for byte alignment?
};

struct lighting
{
	float ambient[4];
	float diffuse[4];
	float specular[4];
};

struct material
{
	float ambient[4];
	float diffuse[4];
	float specular[4];
	float shininess;
};

//struct Vector3
//{
//	float x;
//	float y;
//	float z;
//	//float padding? for byte alignment?
//
//	Vector3()
//	{
//		x = 0.0f;
//		y = 0.0f;
//		z = 0.0f;
//	}
//
//	Vector3(float initialX, float initialY, float initialZ)
//	{
//		x = initialX;
//		y = initialY;
//		z = initialZ;
//	}
//
//	Vector3 operator+ (const Vector3 b)
//	{
//		Vector3 result;
//		result.x = x + b.x;
//		result.y = y + b.y;
//		result.z = z + b.z;
//		return result;
//	}
//
//	Vector3 operator- (const Vector3 b)
//	{
//		Vector3 result;
//		result.x = x - b.x;
//		result.y = y - b.y;
//		result.z = z - b.z;
//		return result;
//	}
//
//	Vector3& operator+= (const Vector3 b)
//	{
//		x = x + b.x;
//		y = y + b.y;
//		z = z + b.z;
//		return *this;
//	}
//
//	Vector3& operator-= (const Vector3 b)
//	{
//		x = x - b.x;
//		y = y - b.y;
//		z = z - b.z;
//		return *this;
//	}
//
//	Vector3 operator* (const float b) //multiply by scalar value
//	{
//		Vector3 result;
//		result.x = x*b;
//		result.y = y *b;
//		result.z = z*b;
//		return result;
//	}
//
//	Vector3& operator*= (const float b)
//	{
//		x = x*b;
//		y = y*b;
//		z = z*b;
//		return *this;
//	}
//
//	Vector3 operator/ (const float b) //divide by scalar value
//	{
//		Vector3 result;
//		result.x = x / b;
//		result.y = y / b;
//		result.z = z / b;
//		return result;
//	}
//};

//the object type
typedef struct
{
	char name[20];
	int verticesQuantity;
	int polygonsQuantity;
	Vertex3D vertex[MAX_VERTICES];
	Triangle polygon[MAX_POLYGONS];
	TexCoord mapCoord[MAX_VERTICES];
	int textureID;
}objType, *pObjType;
