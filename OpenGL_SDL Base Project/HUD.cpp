#include "HUD.h"

using namespace::std;

HUD::HUD()
{
	_texture = new Texture2D;
}

HUD::~HUD()
{
	delete _texture;
}

void HUD::Output(float x, float y, string text, bool highlighted, Vector3 colour)
{



	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	glLoadIdentity();

	gluOrtho2D(0, 100, 0, 100);

	glColor3f(1.0f, 0.0f, 0.0f);

	if (highlighted)
	{
		glColor3f(colour.x, colour.y, colour.z);
	}
	glRasterPos2f(x, y);
	for (int i = 0; i < text.size(); i++)
	{
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, text[i]);
	}
	
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void HUD::DrawTextured2DSquare(float x, float y)
{
	glBindTexture(GL_TEXTURE_2D, _texture->GetID());

	glBegin(GL_QUADS);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(x-0.5, y-0.5f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(x+0.5, y-0.5f, 0.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(x+0.5, y+0.5f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(x-0.5, y+0.5f, 0.0f);

	glEnd();
}

void HUD::LoadTexture(char* path, int width, int height)
{
	_texture->Load(path, width, height);
	glBindTexture(GL_TEXTURE_2D, _texture->GetID());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void HUD::LoadTGA(char* path)
{
	_texture->LoadTGA(path);
	glBindTexture(GL_TEXTURE_2D, _texture->GetID());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}