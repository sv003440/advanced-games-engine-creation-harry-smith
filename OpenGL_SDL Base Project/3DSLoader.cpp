#include <stdio.h> 
#include <stdlib.h> 
#include "Commons.h"
#include "3dsLoader.h" 
#include <sys\stat.h>

long fileLength(int f)
{
	struct stat buf;

	fstat(f, &buf);

	return (buf.st_size);
}

char Load3DS(pObjType pObject, char* pFilename)
{
	int index; //index variable
	FILE* inFile; //file pointer

	unsigned short chunkID, temp; //chunk identifier unsigned
	int chunkLength;
	unsigned char name;
	unsigned short numElementsInChunk;

	unsigned short faceFlag; //flag that stores some face information

	if ((inFile = fopen(pFilename, "rb")) == NULL)
		return 0;

	while (ftell(inFile) < fileLength(fileno(inFile)))
	{
		fread(&chunkID, 2, 1, inFile);  //reads the chunk header (2 bytes)
		fread(&chunkLength, 4, 1, inFile); //reads the length of the chunk (4 bytes)
		//getchar(); //for debug purposes, so that the progam will wait for a keypress before continuing

		switch (chunkID)
		{
			//-----------------MAIN3DS ---------------// 
			//Description: Main chunk, contains all the other chunks 
			// Chunk ID: 4d4d // Chunk Length: 0 + sub chunks 
			//-------------------------------------
		case 0x4d4d:
			break;

			//--	EDIT3DS
			// Description: 3D Editor chunk, objects layout info
			// Chunk ID: 3d3d (hex)
			// Chunk Length: 0 + sub chunks
		case 0x3d3d:
			break;

			//	EDIT_OBJECT--
			//	Description: Object block, info for each object
		 // Chunk ID: 4000 (hex)
		 // Chunk Length: len(object name) + sub chunks
			//--
		case 0x4000:
			index = 0;
			do
			{
				fread(&name, 1, 1, inFile);
				pObject->name[index] = name;
				index++;
			} while (name != '\0' && index < 20);
			break;

			//--	OBJ_TRIMESH--
			//Description: Triangular mesh, contains chunks for 3d mesh info
			// Chunk ID: 4100 (hex)
			 // Chunk Length: 0 + sub chunks
			//--
			
		case 0x4100:
			break;

			//--TRI_VERTEXL--
			//Description: Vertices list
			// Chunk ID: 4110 (hex)
			 // Chunk Length: 1 x unsigned short (number of vertices)
			 // + 3 x float (vertex coordinates) x (number of vertices)
			 // + sub chunks
			 //--
		case 0x4110:
			fread(&numElementsInChunk, sizeof(unsigned short), 1, inFile);
			pObject->verticesQuantity = numElementsInChunk;

			for (index = 0; index < numElementsInChunk; index++)
			{
				fread(&pObject->vertex[index].x, sizeof(float), 1, inFile);
				fread(&pObject->vertex[index].y, sizeof(float), 1, inFile);
				fread(&pObject->vertex[index].z, sizeof(float), 1, inFile);
			}
			break;

			//--	TRI_FACEL1--
				//	Description: triangles(faces) list
			 // Chunk ID: 4120 (hex)
			 // Chunk LengtH: 1 x unsigned short (number of triangles)
			 // + 3 x unsigned short (triangle points) x(number of triangles)
		 // + sub chunks
		 //--
		case 0x4120:
			fread(&numElementsInChunk, sizeof(unsigned short), 1, inFile);
			pObject->polygonsQuantity = numElementsInChunk;

			for (index = 0; index < numElementsInChunk; index++)
			{
				fread(&temp, sizeof(unsigned short), 1, inFile);
				pObject->polygon[index].a = temp;

				fread(&temp, sizeof(unsigned short), 1, inFile);
				pObject->polygon[index].b = temp;

				fread(&temp, sizeof(unsigned short), 1, inFile);
				pObject->polygon[index].c = temp;

				fread(&faceFlag, sizeof(unsigned short), 1, inFile);

			}
			break;

				//TRI_	MAPPINGCOORS--
				//Description: Vertices list
				// Chunk ID: 4140 (hex)
				// Chunk Length: 1 x unsigned short (number of mapping points)
				// + 2 x float (mapping coordinates) x (number of mapping points)
				// + sub chunks
				 //--
		case 0x4140:
			fread(&numElementsInChunk, sizeof(unsigned short), 1, inFile);
			for (index = 0; index < numElementsInChunk; index++)
			{
				fread(&pObject->mapCoord[index].u, sizeof(float), 1, inFile);
				fread(&pObject->mapCoord[index].v, sizeof(float), 1, inFile);
			}
			break;

			//Skip unknown chunks--
			//We need to skip all the chunks that currently we don't use
			//We use the chunk lenth information to set the file pointer
			//to the same level next chunk
			//--
		default:
			fseek(inFile, chunkLength - 6, SEEK_CUR);
		}
	}
	fclose(inFile);
	return(1);
}