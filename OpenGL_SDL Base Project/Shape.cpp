#include "Shape.h"
#include <fstream>
#include <iostream>
#include <string>
using namespace::std;

Shape::Shape(char* file)
{
	Load(file);
}

Shape::~Shape()
{
	delete indexedVertices;
	delete indices;
	delete colours;
	delete normals;
}

void skip_lines(std::istream& pStream)
{
	std::string s;
	std::getline(pStream, s);
}

bool Shape::Load(char* path)
{
	ifstream inFile;
	inFile.open(path);

	if (!inFile.good())
	{
		cerr << "Can't open model file " << path << endl;
		return false;
	}

	skip_lines(inFile);
	inFile >> numVertices;
	if (numVertices > 0)
	{
		indexedVertices = new Vertex3D[numVertices];
		for (int i = 0; i < numVertices; i++)
		{
			inFile >> indexedVertices[i].x;
			inFile >> indexedVertices[i].y;
			inFile >> indexedVertices[i].z;
		}
	}

	skip_lines(inFile);
	skip_lines(inFile);

	inFile >> numNormals;
	normals = new Vector3[numNormals];
	for (int i = 0; i < numNormals; i++)
	{
		inFile >> normals[i].x;
		inFile >> normals[i].y;
		inFile >> normals[i].z;
	}

	int numTriangles;
	skip_lines(inFile);
	skip_lines(inFile);
	inFile >> numTriangles;
	indexCount = numTriangles * 3;

	if (indexCount > 0)
	{
		indices = new short[indexCount];
		for (int i = 0; i < indexCount; i++)
		{
			inFile >> indices[i];
		}
	}

	int numColours;
	skip_lines(inFile);//used for skipping comments in the txt
	skip_lines(inFile);
	inFile >> numColours;

	if (numColours > 0)
	{
		colours = new Colour[numColours];

		for (int i = 0; i < numColours; i++)
		{
			inFile >> colours[i].r;
			inFile >> colours[i].g;
			inFile >> colours[i].b;
		}
	}

	inFile.close();
	return true;
}

void Shape::Draw()
{
	for (int i = 0; i < indexCount; i += 3)
	{
		
		DrawTriangle(indices[i], indices[i + 1], indices[i + 2]);
	}
}

void Shape::DrawTriangle(short a, short b, short c)
{
	glBegin(GL_TRIANGLES);
	//glColor3f(colours[a].r, colours[a].g, colours[a].b);
	glNormal3f(normals[a].x, normals[a].y, normals[a].z);
	glVertex3f(indexedVertices[a].x, indexedVertices[a].y, indexedVertices[a].z);
	//glColor3f(colours[b].r, colours[b].g, colours[b].b);
	glNormal3f(normals[b].x, normals[b].y, normals[b].z);
	glVertex3f(indexedVertices[b].x, indexedVertices[b].y, indexedVertices[b].z);
	//glColor3f(colours[c].r, colours[c].g, colours[c].b);
	glNormal3f(normals[c].x, normals[c].y, normals[c].z);
	glVertex3f(indexedVertices[c].x, indexedVertices[c].y, indexedVertices[c].z);
	glEnd();
}