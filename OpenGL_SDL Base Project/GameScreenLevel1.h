#ifndef _GAMESCREENLEVEL1_H
#define _GAMESCREENLEVEL1_H

#include "GameScreen.h"
#include "Ball.h"


class GameScreenLevel1 : public GameScreen
{
//--------------------------------------------------------------------------------------------------
public:
	GameScreenLevel1();
	~GameScreenLevel1();

	bool		SetUpLevel();
	void		Render();
	void		Update(float deltaTime, SDL_Event e);
	SCREENS GoToNextLevel() override;
	bool IsGameOver()override;
//--------------------------------------------------------------------------------------------------
private:
	
	float _currentTime;

	void	SetLight();
	void	SetMaterial();

	std::vector<Object3DS*> _objects;
	std::vector<Ball*> _balls;

	int numberOfBalls;
	int _ballHitCount = 0;

	Object3DS* _denzil;

	HUD* _myHud;
	HUD* _backgroundLeft;
	HUD* _backgroundRight;

	Mix_Music* _backgroundMusic;
	Mix_Chunk* _triumph;

	Camera* _camera;

	Sphere* _endPointCollision;

	bool EndPointReached();
	void BallHitDenzil();
};


#endif //_GAMESCREENLEVEL1_H