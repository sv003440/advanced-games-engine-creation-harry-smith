#include "SplashScreenLevel2.h"
#include "GameScreenManager.h"

SplashScreenLevel2::SplashScreenLevel2(GameScreenManager* GSManager) : GameScreen(), _GSManager(GSManager)
{
	glDisable(GL_LIGHTING);

	glDisable(GL_NORMALIZE);

	glDisable(GL_TEXTURE_2D);

	_camera = _camera->GetInstance();
	_message = new HUD();
}

SplashScreenLevel2::~SplashScreenLevel2()
{
	delete _message;
}

void SplashScreenLevel2::Render()
{
	
	GameScreen::Render();

	_message->Output(20.0f, 50.0f, "Now the game is reversed!");
	_message->Output(20.0f, 40.0f, "Collect as many balls as possible in the alotted time!");
	_message->Output(20.0f, 30.0f, "Press Enter to proceed to Level 2");
}

void SplashScreenLevel2::Update(float deltaTime, SDL_Event e)
{
	if (e.type == SDL_KEYUP)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_RETURN:
			_GSManager->ChangeScreen(SCREENS::LEVEL2);
			break;
		}
	}
}

