#pragma once
#include "GameScreen.h"
#include "Ball.h"


class GameScreenLevel2 : public GameScreen
{
public:
	GameScreenLevel2();
	~GameScreenLevel2();

	void Render();
	void Update(float deltaTime, SDL_Event e);
	SCREENS GoToNextLevel() override;
	bool IsGameOver()override;

private:
	HUD* _myHud;
	float _timeLimit;



private:
	bool SetUpLevel();
	float _currentTime;

	void	SetLight();
	void	SetMaterial();

	std::vector<Object3DS*> _objects;
	std::vector<Ball*> _balls;

	int numberOfBalls;
	int _ballHitCount = 0;

	Object3DS* _denzil;

	HUD* _backgroundLeft;
	HUD* _backgroundRight;

	Mix_Music* _backgroundMusic;
	Mix_Chunk* _triumph;

	Camera* _camera;

	//Sphere* _endPointCollision;

//	bool EndPointReached();
	void BallHitDenzil();
	
};