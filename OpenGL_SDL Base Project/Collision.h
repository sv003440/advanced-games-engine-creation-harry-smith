#pragma once
// Collision - library of functions and classes for collision detection
#include "Commons.h"
#include "Vector3.h"
#include<string>

class Sphere {
public:
	Sphere(Vector3 c, float r, string owner) : centre(c), radius(r), collided(false), oldPos(Vector3()), _owner(owner) {};// { centre = c; radius = r; collided = false; 	oldPos = Vector3(0.0f, 0.0f, 0.0f);	}
	
	float GetBoundingRadius() const { return radius; }
	
	Vector3 GetCentre()  { return centre;  }
	
	void Update(Vector3 newPos, Vector3 velocity = Vector3()) { 
		centre = newPos; 
		this->velocity = velocity;
	}

	void SetCollided(bool c) { collided = c; };
	bool GetCollided() { return collided; };

	Vector3 getVelocity() const { return velocity; };
	void setVelocity(Vector3 newVel) { velocity = newVel; };

	string GetOwner() const { return _owner; };

	void SetCollidedWith(string name) { collidedWith = name; };
	string GetCollidedWith() const { return collidedWith; };

	Sphere* getOtherSphere() const { return _otherSphere; };
	void SetOtherSphere(Sphere* other) { _otherSphere = other; };

private:
	Vector3 centre;
	float radius;
	bool collided;
	Vector3 oldPos;
	Vector3 velocity;
	string _owner;
	string collidedWith;

	Sphere* _otherSphere;
};

class Box
{
public:
	void Update(Vector3 newPos);
	float GetX() const { return posX; };
	float GetY() const { return posY; };
	float GetWidth() const { return width; };
	float GetHeight() const { return height; };


private:
	Vector2D bottomLeftCorner;
	float posX, posY;
	float width, height;
};

class Collision {
public:

	static bool SphereSphereCollision(Sphere *s1, Sphere *s2) {
		Vector3 centre1 = s1->GetCentre();
		Vector3 centre2 = s2->GetCentre();
		float dx = centre2.x - centre1.x;
		float dy = centre2.y - centre1.y;
		float dz = centre2.z - centre1.z;

		float dSquared = dx*dx + dy*dy + dz *dz;

		float sumOfBoundingRadii = s1->GetBoundingRadius() + s2->GetBoundingRadius();

		float hasCollided = false;
		if (dSquared < (sumOfBoundingRadii * sumOfBoundingRadii))
		{
			s1->SetCollided(true);
			s2->SetCollided(true);

			s1->SetCollidedWith(s2->GetOwner());
			s2->SetCollidedWith(s1->GetOwner());

			s1->SetOtherSphere(s2);
			s2->SetOtherSphere(s1);
			float totMass = s1->GetBoundingRadius() + s2->GetBoundingRadius();


			Vector3 finalVelocity1 = (s1->getVelocity() *s1->GetBoundingRadius()
				+ s2->getVelocity() * s2->GetBoundingRadius()
				+ ((s2->getVelocity() - s1->getVelocity()) * s2->GetBoundingRadius() * 0.9f))
				/ totMass;
			Vector3 finalVelocity2 = (s2->getVelocity() *s2->GetBoundingRadius()
				+ s1->getVelocity() * s1->GetBoundingRadius()
				+ ((s1->getVelocity() - s2->getVelocity()) * s1->GetBoundingRadius() * 0.9f))
				/ totMass;
			s1->setVelocity(finalVelocity1);
			s2->setVelocity(finalVelocity2);

			return true;
		}
		return false;
	}

};
