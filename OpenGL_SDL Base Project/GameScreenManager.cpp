#include "GameScreenManager.h"
#include "GameScreen.h"
#include "GameScreenLevel1.h"
#include "GameScreenLevel2.h"
#include "GameScreenMenu.h"
#include "SplashScreenLevel1.h"
#include "SplashScreenLevel2.h"
#include "GameOverScreen.h"

//--------------------------------------------------------------------------------------------------

GameScreenManager::GameScreenManager(SCREENS startScreen) : _quit{false}, _currentScreen{nullptr}
{
	//Ensure the first screen is set up.
	ChangeScreen(startScreen);
}

//--------------------------------------------------------------------------------------------------

GameScreenManager::~GameScreenManager()
{
	delete _currentScreen;
	_currentScreen = nullptr;
}

//--------------------------------------------------------------------------------------------------

void GameScreenManager::Render()
{
	_currentScreen->Render();
}

//--------------------------------------------------------------------------------------------------

void GameScreenManager::Update(float deltaTime, SDL_Event e)
{
	if (_currentScreen->IsGameOver())
	{
		ChangeScreen(SCREENS::GAMEOVER);
	}

	SCREENS temp = _currentScreen->GoToNextLevel();
	if (temp != SCREENS::NONE)
	{
		ChangeScreen(temp);
	}

	_currentScreen->Update(deltaTime, e);
}

//--------------------------------------------------------------------------------------------------

void GameScreenManager::ChangeScreen(SCREENS newScreen)
{
	//Clear up the old screen.
	if(_currentScreen)
	{
		delete _currentScreen;
	}

	GameScreen* tempScreen1;
	GameScreenMenu* startScreen;
	//Initialise the new screen.
	switch(newScreen)
	{
	case SCREENS::INTRO:
		break;

		case SCREENS::MENU:
			startScreen = new GameScreenMenu(this);
			_currentScreen = startScreen;
			startScreen = nullptr;
		break;

		case SCREENS::SPLASHSCREENLEVEL1:
			tempScreen1 = new SplashScreenLevel1(this);
			_currentScreen = tempScreen1;
			tempScreen1 = nullptr;
			break;

		case SCREENS::LEVEL1:
			tempScreen1 = new GameScreenLevel1();
			_currentScreen = tempScreen1;
			tempScreen1 = nullptr;
		break;

		case SCREENS::SPLASHSCREENLEVEL2:
			tempScreen1 = new SplashScreenLevel2(this);
			_currentScreen = tempScreen1;
			tempScreen1 = nullptr;
			cout << "splash 2 loaded\n";
			break;

		case SCREENS::LEVEL2:
			tempScreen1 = new GameScreenLevel2();
			_currentScreen = tempScreen1;
			tempScreen1 = nullptr;
			cout << "LEVEL 2 loaded\n";
			break;

		
		case SCREENS::GAMEOVER:
			tempScreen1 = new GameOverScreen(this);
			_currentScreen = tempScreen1;
			tempScreen1 = nullptr;
		break;
		
		case SCREENS::HIGHSCORES:
		break;
		
		default:
		break;
	}
}

//--------------------------------------------------------------------------------------------------