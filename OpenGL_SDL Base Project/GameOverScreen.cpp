#include "GameOverScreen.h"
#include "GameScreenManager.h"

GameOverScreen::GameOverScreen(GameScreenManager* GSManager) : GameScreen(), _GSManager(GSManager)
{
	glDisable(GL_LIGHTING);

	glDisable(GL_NORMALIZE);

	glDisable(GL_TEXTURE_2D);

	_camera = _camera->GetInstance();
	_message = new HUD();
}

GameOverScreen::~GameOverScreen()
{
	delete _message;
}

void GameOverScreen::Render()
{
	GameScreen::Render();

	_message->Output(20.0f, 50.0f, "Game Over!");
	_message->Output(20.0f, 30.0f, "Press Enter to proceed to go to the Main Menu");
}

void GameOverScreen::Update(float deltaTime, SDL_Event e)
{
	if (e.type == SDL_KEYUP)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_RETURN:
			_GSManager->ChangeScreen(SCREENS::MENU);
			break;
		}
	}
}
