#include "GameScreenLevel1.h"

using namespace::std;

//--------------------------------------------------------------------------------------------------

GameScreenLevel1::GameScreenLevel1() : GameScreen(), numberOfBalls(10), _currentTime(0.0f)
{
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

	glEnable(GL_NORMALIZE);

	glEnable(GL_TEXTURE_2D);

	_myHud = new HUD();

	_camera = _camera->GetInstance();

	_backgroundMusic = Mix_LoadMUS("Sounds\\Music\\01_-_Heroic_Fantasy.mp3");
	_triumph = Mix_LoadWAV("Sounds\\bam_bam_bolam.wav");

	if (!_backgroundMusic)
	{
		std::cerr << "background music failed to load\n";
	}

	Mix_PlayMusic(_backgroundMusic, -1);

	if (!SetUpLevel())
		std::cerr << "failed to load level\n";
}

//--------------------------------------------------------------------------------------------------

GameScreenLevel1::~GameScreenLevel1()
{	
	delete _myHud;
	delete _backgroundLeft;
	delete _backgroundRight;
	delete _denzil;

	Mix_FreeMusic(_backgroundMusic);
	Mix_FreeChunk(_triumph);
}


void GameScreenLevel1::SetLight()
{
	lighting light = 
	{
		{ 0.2f, 0.2f, 0.2f, 1.0f },
		{ 0.7f, 0.7f, 0.7f, 1.0f },
		{ 0.5f, 0.5f, 0.5f, 1.0f }
	};
	// position of the light in homogeneous coordinates (x, y, z, w)
	// w should be 0 for directional lights, and 1 for spotlights
	float light_pos[] = { -10.0f, 10.0f, 1.0f, 0.0f };

	glLightfv(GL_LIGHT0, GL_AMBIENT, light.ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light.diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light.specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

	lighting light2 =
	{
		{ 0.2f, 0.2f, 0.2f, 1.0f },
		{ 0.0f, 0.7f, 0.9f, 1.0f },
		{ 0.0f, 0.0f, 0.9f, 1.0f }
	};
	// position of the light in homogeneous coordinates (x, y, z, w)
	// w should be 0 for directional lights, and 1 for spotlights
	float light_pos2[] = { 1.0f, 5.0f, 1.0f, 0.0f };

	glLightfv(GL_LIGHT1, GL_AMBIENT, light2.ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light2.diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light2.specular);
	glLightfv(GL_LIGHT1, GL_POSITION, light_pos2);
}

void GameScreenLevel1::SetMaterial()
{
	material material = 
	{
		{ 1.0f, 1.0f, 1.0f, 1.0f },
		{ 1.0f, 1.0f, 1.0f, 1.0f },
		{ 1.0f, 1.0f, 1.0f, 1.0f },
		10.0f
	};

	glMaterialfv(GL_FRONT, GL_AMBIENT, material.ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material.diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material.specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material.shininess);
}

void GameScreenLevel1::Render() 
{
	GameScreen::Render();

	glDisable(GL_LIGHTING);

	_myHud->Output(0, 95, "Current time: " + to_string(floor(_currentTime)) + " seconds\t Number of Balls Hit: " + to_string(_ballHitCount));

	glEnable(GL_LIGHTING);

	SetLight();
	SetMaterial();

	_camera->Render();

	_backgroundLeft->DrawTextured2DSquare(-0.5f, 0.0f);
	_backgroundRight->DrawTextured2DSquare(0.5f, 0.0f);

	for (int i = 0; i < _balls.size(); ++i)
	{
		_balls[i]->Render();
	}

	_denzil->Render();
}

//--------------------------------------------------------------------------------------------------

void GameScreenLevel1::Update(float deltaTime, SDL_Event e)
{
	_currentTime += deltaTime;

	_camera->Update(deltaTime, e);

	_denzil->HandleInput(deltaTime, e);

	for (int i = 0; i < _objects.size(); i++)
	{
		_objects[i]->Update(deltaTime);
	}
	for (int i = 0; i < _balls.size(); ++i)
	{
		if (!_balls[i]->HasHitDenzil())
		{
			if (Collision::SphereSphereCollision(_balls[i]->GetBoundingSphere(), _denzil->GetBoundingSphere()))
			{
				_denzil->HitByBall();
				BallHitDenzil();
			}
		}
		_balls[i]->Update(deltaTime);
	}	
}

bool GameScreenLevel1::SetUpLevel()
{
	_denzil = new Object3DS(Vector3(-1.0f, -0.5f, 0.1f), 0.01f,"Models\\DodgeBall\\boar.3DS", "Denzil");
	_denzil->LoadTextureTGA("Textures\\DodgeBall\\boar.tga");
	_denzil->SetRotation(180);
	_objects.push_back(_denzil);

	_backgroundLeft = new HUD();
	_backgroundRight = new HUD();

	_backgroundLeft->LoadTGA("Textures\\DodgeBall\\Court.tga");
	_backgroundRight->LoadTGA("Textures\\DodgeBall\\Court2.tga");

	_endPointCollision = new Sphere(Vector3(1.0f, 0.5f, 0.1f), 0.2f, "level");
	
	for (int i = 0; i < numberOfBalls; ++i)
	{
		Ball* tempBall = new Ball(0.05, Vector3(0.0f, 0.0f, -10.0f), 1.0f, "ball" + i);

		_balls.push_back(tempBall);
	}

	return true;
}

bool GameScreenLevel1::EndPointReached()
{
	return (Collision::SphereSphereCollision(_denzil->GetBoundingSphere(), _endPointCollision));
}

void GameScreenLevel1::BallHitDenzil()
{
	++_ballHitCount;
	cout << _ballHitCount << '\n';
}

SCREENS GameScreenLevel1::GoToNextLevel()
{
	//return EndPointReached();
	if (EndPointReached())
	{
		Mix_PlayChannel(-1, _triumph, 1);
		
		cout << "Proceed to next level\n";
		return SCREENS::SPLASHSCREENLEVEL2;
	}
	return SCREENS::NONE;
}

bool GameScreenLevel1::IsGameOver()
{
	return _denzil->CheckDeath();
}
//--------------------------------------------------------------------------------------------------