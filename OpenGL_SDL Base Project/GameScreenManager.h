#ifndef _GAMESCREENMANAGER_H
#define _GAMESCREENMANAGER_H

#include <SDL.h>
#include <vector>
#include "Commons.h"

class GameScreen;

class GameScreenManager
{
public:
	GameScreenManager() {};
	GameScreenManager(SCREENS startScreen);
	~GameScreenManager();

	void Render();
	void Update(float deltaTime, SDL_Event e);

	void ChangeScreen(SCREENS newScreen);

	bool CheckQuit()const { return _quit; };
	void SendQuitMessage() { _quit = true; }
private:
	GameScreen* _currentScreen;

	bool _quit;

};


#endif //_GAMESCREENMANAGER_H