#include "SplashScreenLevel1.h"
#include "GameScreenManager.h"

SplashScreenLevel1::SplashScreenLevel1(GameScreenManager* GSManager) : GameScreen(), _GSManager(GSManager)
{
	glDisable(GL_LIGHTING);

	glDisable(GL_NORMALIZE);

	glDisable(GL_TEXTURE_2D);

	_camera = _camera->GetInstance();
	_message = new HUD();
}

SplashScreenLevel1::~SplashScreenLevel1()
{
	delete _message;
}

void SplashScreenLevel1::Render()
{
	GameScreen::Render();

	_message->Output(20.0f, 50.0f,"Use the arrow keys to move Denzil"); 
	_message->Output(20.0f, 40.0f, "Avoid the balls and reach the other end of the court to win!");
	_message->Output(20.0f, 30.0f, "Press Enter to proceed to Level 1");
}

void SplashScreenLevel1::Update(float deltaTime, SDL_Event e)
{
	if (e.type == SDL_KEYUP)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_RETURN:
			_GSManager->ChangeScreen(SCREENS::LEVEL1);
			break;
		}
	}
}

