#pragma once
#include "GameScreen.h"
#include "HUD.h"
#include "Camera.h"

class GameScreenManager;

class SplashScreenLevel1 : public GameScreen
{
public:
	SplashScreenLevel1(GameScreenManager* GSManager);
	~SplashScreenLevel1();

	void Render() override;
	void Update(float deltaTime, SDL_Event e) override;

private:
	GameScreenManager* _GSManager;
	Camera* _camera;
	HUD* _message;

};
