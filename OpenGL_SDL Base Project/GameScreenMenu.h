#pragma once
#include "GameScreen.h"
#include "HUD.h"
#include "Camera.h"

class GameScreenManager;
enum class Options
{
	NONE,//NONE should always be first
	LEVEL1,
	LEVEL2,
	QUIT,//QUIT should always be last, even if more levels are added. They should be added before quit
};

class GameScreenMenu : public GameScreen
{
public:
	GameScreenMenu(GameScreenManager* GSManager);
	~GameScreenMenu();

	void Render() override;
	void Update(float deltaTime, SDL_Event e) override;
	

private:
	HUD* _menuMessage;
	Camera* _camera;

	void ChooseOption(SDL_Event e);
	bool HighlightThis(Options option);
	
	int _currentlySelected;

	GameScreenManager* _GSManager;
};


