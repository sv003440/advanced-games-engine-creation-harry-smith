#include "Camera.h"
#include "Constants.h"
#include "Vector3.h"
#include <math.h>
#include "../gl/glut.h"

static Camera* instance = 0;
static float moveSpeed = 1.0f;
static float lookSpeed = 0.1f;
Camera::Camera()
{
}
Camera::~Camera()
{
}

Camera* Camera::GetInstance()
{
	if (!instance)
	{
		instance = new Camera();
	}

	return instance;
}

void Camera::Update(float deltaTime, SDL_Event e)
{
	// Forward Vector: Spherical coordinates to Cartesian coordinates conversion(also known as the �look� direction)
	_forward = Vector3(cos(_verticalAngle) * sin(_horizontalAngle), sin(_verticalAngle), cos(_verticalAngle) * cos(_horizontalAngle));
	//right vector
	_right = Vector3(sin(_horizontalAngle - 3.14f / 2.0f), 0, cos(_horizontalAngle - 3.14f / 2.0f));
	//up vector: calculated with cross product as it is perpendicuar to the previous vectors
	_up = Vector3((_right.y*_forward.z) - (_right.z*_forward.y), (_right.z*_forward.x) - (_right.x*_forward.z), (_right.x*_forward.y) - (_right.y*_forward.x));

	//event handler
	if (e.type == SDL_KEYUP)
	{
		switch (e.key.keysym.sym)
		{
			//movement
		case SDLK_w:
			_position += _forward * moveSpeed;
			break;
		case SDLK_s:
			_position -= _forward * moveSpeed;
			break;
		case SDLK_d:
			_position += _right * moveSpeed;
			break;
		case SDLK_a:
			_position -= _right * moveSpeed;
			break;
			//look angle:
		case SDLK_PAGEUP:
			_verticalAngle += lookSpeed;
			break;
		case SDLK_PAGEDOWN:
			_verticalAngle -= lookSpeed;
			break;
		case SDLK_e:
			_horizontalAngle += lookSpeed;
			break;
		case SDLK_q:
			_horizontalAngle -= lookSpeed;
			break;
		case SDLK_LEFTBRACKET:
			_position -= _right * moveSpeed;
			_horizontalAngle -= lookSpeed;
			break;
		case SDLK_RIGHTBRACKET:
			_position += _right * moveSpeed;
			_horizontalAngle += lookSpeed;
			break;
		case SDLK_INSERT:
			_position += _up * moveSpeed;
			break;
		case SDLK_DELETE:
			_position -= _up * moveSpeed;
			break;

		case SDLK_HOME:
			_position += _up * moveSpeed;
			_verticalAngle -= lookSpeed;
			break;
		case SDLK_END:
			_position -= _up * moveSpeed;
			_verticalAngle += lookSpeed;
			break;

		default:
			break;
		}
	}
}

void Camera::Render()
{
	Vector3 lookAt = _position + _forward;
	glLoadIdentity();
	gluLookAt(_position.x, _position.y, _position.z, lookAt.x, lookAt.y, lookAt.z, _up.x, _up.y, _up.z);
}

