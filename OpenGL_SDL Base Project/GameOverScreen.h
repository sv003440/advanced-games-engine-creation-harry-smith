#pragma once
#include "GameScreen.h"
#include "HUD.h"
#include "Camera.h"

class GameScreenManager;

class GameOverScreen : public GameScreen
{
public:
	GameOverScreen(GameScreenManager* GSManager);
	~GameOverScreen();

	void Render() override;
	void Update(float deltaTime, SDL_Event e) override;

private:
	GameScreenManager* _GSManager;
	Camera* _camera;
	HUD* _message;

};
