#ifndef _GAMESCREEN_H
#define _GAMESCREEN_H

#include <SDL.h>
#include <time.h>
#include <windows.h>
#include <GL\gl.h>
#include <GL\glu.h>
#include "../gl/glut.h"
#include "Constants.h"
#include "Commons.h"
#include "Texture2D.h"
#include <iostream>
#include <vector>
#include "Vector3.h"
#include <SDL_mixer.h>
#include "Collision.h"
#include "object3DS.h"
#include "HUD.h"
#include "Camera.h"


class GameScreen
{
public:
	GameScreen();
	virtual ~GameScreen();

	virtual void Render();
	virtual void Update(float deltaTime, SDL_Event e);

	virtual SCREENS GoToNextLevel() { return SCREENS::NONE; };
	virtual bool IsGameOver() { return false; }

protected:
	
};


#endif //_GAMESCREEN_H