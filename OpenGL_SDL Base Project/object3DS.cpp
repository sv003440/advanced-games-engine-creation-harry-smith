#include "object3DS.h" 
#include "../gl/glut.h" 
#include "3dsLoader.h"
#include <iostream>

Object3DS::Object3DS(Vector3 startPosition, float startScale, string modelFileName, string name) : _position(startPosition), _scale(Vector3(startScale, startScale, startScale)), _name(name), _screamVolume(20)
{
	_texture = new Texture2D;
	

	std::strcpy(_fileName, modelFileName.c_str());
	if (!LoadModel())
	{
		std::cerr << "The model " << _fileName << " failed to load\n";
	}

	_scream = Mix_LoadWAV("Sounds\\Scream.wav");
	Mix_VolumeChunk(_scream, _screamVolume);
	_moveSpeed = 1.0f;
	_collisionSphere = new Sphere(startPosition, 0.06f, name);
}

Object3DS::~Object3DS()
{
	delete _texture;
}


bool Object3DS::LoadModel()
{
	if (_fileName[0] != '---')
	{
		Load3DS(&_object, _fileName);
		return true;
	}
	else return false;
}

void Object3DS::LoadTexture(char* path, int width, int height)
{
	_texture->Load(path, width, height);
	glBindTexture(GL_TEXTURE_2D, _texture->GetID());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void Object3DS::LoadTextureTGA(char* path)
{
	_texture->LoadTGA(path);
	glBindTexture(GL_TEXTURE_2D, _texture->GetID());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void Object3DS::Update(float deltaTime)
{
	float multiplierY = _position.y < 0 ? -1.0f : 1.0f;
	multiplierY *= _moveSpeed * deltaTime;
	float multiplierX = _position.x < 0 ? -1.0f : 1.0f;
	multiplierX *= _moveSpeed * deltaTime;
 
	if (abs(_position.x) > 1.0f)
	{
		MoveRight(multiplierX * -1);
	}
	if (abs(_position.y) > 0.5f)
	{
		MoveUp(multiplierY * -1);
	}

	_collisionSphere->Update(_position);
	_collisionSphere->SetCollided(false);
}

void Object3DS::Render()
{

	glPushMatrix();

	glTranslatef(_position.x, _position.y, _position.z);
	glRotatef(_rotAngle, 0.0f, 0.0f, 1.0f);
	glScalef(_scale.x, _scale.y, _scale.z);
	//glBindTexture(GL_TEXTURE_2D, object.textureID); // We set the active texture
	glBindTexture(GL_TEXTURE_2D, _texture->GetID());

	glBegin(GL_TRIANGLES); // glBegin and glEnd delimit the vertices that define a primitive (in our case triangles)
	for (int i = 0; i < _object.polygonsQuantity; i++)
	{
		//First Vertex
		// Texture coordinates of the first vertex 
		glTexCoord2f(_object.mapCoord[_object.polygon[i].a].u, _object.mapCoord[_object.polygon[i].a].v);
		// Coordinates of the first vertex
		glVertex3f(_object.vertex[_object.polygon[i].a].x, _object.vertex[_object.polygon[i].a].y, _object.vertex[_object.polygon[i].a].z); //vertex definition

		//Second Vertex
		// Texture coordinates of the second vertex 
		glTexCoord2f(_object.mapCoord[_object.polygon[i].b].u, _object.mapCoord[_object.polygon[i].b].v);
		// Coordinates of the second vertex
		glVertex3f(_object.vertex[_object.polygon[i].b].x, _object.vertex[_object.polygon[i].b].y, _object.vertex[_object.polygon[i].b].z);

		//Third Vertex
		// Texture coordinates of the second vertex 
		glTexCoord2f(_object.mapCoord[_object.polygon[i].c].u, _object.mapCoord[_object.polygon[i].c].v);
		// Coordinates of the third vertex
		glVertex3f(_object.vertex[_object.polygon[i].c].x, _object.vertex[_object.polygon[i].c].y, _object.vertex[_object.polygon[i].c].z);

	}
	glEnd();

	glPopMatrix();

}

void Object3DS::HandleInput(float deltaTime,  SDL_Event event)
{
	if (event.type == SDL_KEYDOWN)
	{
		if (_lastKeyPressed != event.key.keysym.sym)
		{
			_lastKeyPressed = event.key.keysym.sym;
		}
	}

	if (event.type == SDL_KEYUP)
	{
		_lastKeyPressed = 0;
	}

	switch (_lastKeyPressed)
	{
	case SDLK_DOWN:
		MoveUp(deltaTime * -1 * _moveSpeed);
		break;
	case SDLK_UP:
		MoveUp(deltaTime* _moveSpeed);
		break;
	case SDLK_RIGHT:
		MoveRight(deltaTime* _moveSpeed);
		break;
	case SDLK_LEFT:
		MoveRight(deltaTime * -1 * _moveSpeed);
		break;
	}
}

void Object3DS::MoveUp(float amount)
{
	_position.y += amount;
}

void Object3DS::MoveRight(float amount)
{
	_position.x += amount;
}

void Object3DS::SetRotation(float angle)
{
	_rotAngle = angle;
}

void Object3DS::AddRotation(float angle)
{
	_rotAngle += angle;
}

void Object3DS::SetPosition(Vector3 newPosition)
{
	_position = newPosition;
}

void Object3DS::HitByBall()
{
	++_ballHitCount;
}

bool Object3DS::CheckDeath()
{
	if (_ballHitCount > 3)
	{
		Mix_PlayChannel(-1, _scream, 0);
		cout << "DEAD\n";
		return true;
		//Die();
	}
	return false;
}