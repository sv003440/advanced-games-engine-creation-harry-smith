#ifndef _SHAPE_H
#define _SHAPE_H

#include <Windows.h> //required for OpenGL on windows
#include <gl/GL.h> //openGL
#include <gl/GLU.h> //openGL utilities
#include "Commons.h"
#include "Vector3.h"

class Shape
{
private:
	Vertex3D* indexedVertices;
	Colour* colours;
	short* indices;

	int numVertices;
	int indexCount;
	int numNormals;
	Vector3 * normals;


	bool Load(char* path);
	void DrawTriangle(short a, short b, short c);

public:
	Shape(char* file);
	~Shape(void);

	void Draw(void);
	void Update(void);
};

#endif