#ifndef _HUD_H
#define _HUD_H

#include <string>
#include <sstream>
#include <Windows.h> //required for OpenGL on windows
#include <GL\gl.h>
#include <GL\glu.h>
#include "../gl/glut.h"
#include "Texture2D.h"
#include "Vector3.h"

using namespace std;

class HUD
{
private:
	char _textureName[20];
	Texture2D* _texture;

public:
	HUD();
	~HUD();

	void Output(float x, float y, string text, bool highlighted = false, Vector3 colour = Vector3());
	void DrawTextured2DSquare(float x, float y);
	void LoadTexture(char* path, int width, int height);
	void LoadTGA(char* path);
};

#endif