#pragma once
#include <stdlib.h>
#include "../gl/glut.h" 
#include "Vector3.h"
#include "Collision.h"
#include <random>
#include <SDL_mixer.h>

class object3DS;

class Ball
{
public:
	Ball(Vector3 position, double radius, Vector3 startingVelocity, Vector3 gravityForce, float mass);
	Ball(double radius, Vector3 gravityForce, float mass, string name);
	~Ball();

	void Update(float deltaTime);
	void Render();

	Sphere* GetBoundingSphere() const { return _boundingSphere; };
	void ApplyForce(Vector3 force) { _totalForce += force; };

	bool HasHitDenzil() const { return _hitDenzil; };

private:
	Vector3 _position;
	double _radius;
	float _mass;
	string _name;

	Vector3 _velocity;
	Vector3 _acceleration;
	Vector3 _totalForce;
	Vector3 _gravity;
	float _groundHeight;

	Sphere* _boundingSphere;
	Sphere* _denzilSphere;

	Mix_Chunk* _bounceSound;

	void UpdateAcceleration();
	void CheckCollision();
	bool HasHitGround();
	void CheckOutOfBounds();
	void OutOfBounds();

	bool _hitDenzil;
};



