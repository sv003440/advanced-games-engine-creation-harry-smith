#include "GameScreenMenu.h"
#include "GameScreenManager.h"

GameScreenMenu::GameScreenMenu(GameScreenManager* GSManager) : GameScreen(), _GSManager(GSManager), _currentlySelected(0)
{
	glDisable(GL_LIGHTING);

	glDisable(GL_NORMALIZE);

	glDisable(GL_TEXTURE_2D);

	cout << "MENU loaded\n";
	_camera = _camera->GetInstance();

	_menuMessage = new HUD();
}

GameScreenMenu::~GameScreenMenu()
{
	delete _menuMessage;
}

void GameScreenMenu::Render()
{
	GameScreen::Render();

	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);

	_camera->Render();

	_menuMessage->Output(50, 70, "Use up and down arrows to select");
	_menuMessage->Output(50, 60, "Press enter to confirm choice");
	_menuMessage->Output(50, 50, "Level 1", HighlightThis(Options::LEVEL1));
	_menuMessage->Output(50, 40, "Level 2", HighlightThis(Options::LEVEL2));// , Vector3(1.0f, 0.0f, 1.0f));
	_menuMessage->Output(50, 30, "Quit", HighlightThis(Options::QUIT));
}

void GameScreenMenu::Update(float deltaTime, SDL_Event e)
{
	ChooseOption(e);
}

void GameScreenMenu::ChooseOption(SDL_Event e)
{
	if (e.type == SDL_KEYUP)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_DOWN:
			_currentlySelected++;
			break;
		case SDLK_UP:
			_currentlySelected--;
			break;

		case SDLK_RETURN:
			switch (_currentlySelected)
			{
			case (int)Options::NONE:
				break;

			case (int)Options::LEVEL1:
				_GSManager->ChangeScreen(SCREENS::SPLASHSCREENLEVEL1);
				break;

			case (int)Options::LEVEL2:
				_GSManager->ChangeScreen(SCREENS::SPLASHSCREENLEVEL2);
				break;

			case (int)Options::QUIT:
				_GSManager->SendQuitMessage();

				break;
			}
		}

		if (_currentlySelected > (int)Options::QUIT)
			_currentlySelected = 0;
		if (_currentlySelected < 0)
			_currentlySelected = (int)Options::QUIT;
	}
}

bool GameScreenMenu::HighlightThis(Options option)
{
	if (_currentlySelected == (int)option)
	{
		return true;
	}

	return false;
}