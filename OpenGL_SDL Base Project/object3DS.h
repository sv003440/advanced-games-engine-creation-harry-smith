#ifndef _OBJECT3DS_H_ 
#define _OBJECT3DS_H_
#include "Commons.h" 
#include"Texture2D.h"
#include <string> 
#include "Vector3.h"
#include <SDL.h>
#include <SDL_mixer.h>
#include "Collision.h"

using std::string;

class Object3DS
{
public:
	/*Object3DS(Vector3 startPosition, string modelFileName, string name);
	Object3DS(Vector3 startPosition, Vector3 startScale, string modelFileName, string name);
*/
	Object3DS(Vector3 startPosition, float startScale, string modelFileName, string name);
	~Object3DS();

	void Update(float deltaTime);
	void Render();

	bool LoadModel();

	void LoadTexture(char* path, int width, int height);
	void LoadTextureTGA(char* path);

	void MoveUp(float amount);
	void MoveRight(float amount);
	void SetRotation(float angle);
	void AddRotation(float angle);
	void SetPosition(Vector3 newPosition);

	void SetScale(float newScale) { _scale = Vector3(newScale, newScale, newScale); };
	void SetScale(Vector3 newScale) {_scale = newScale;};

	void HandleInput(float deltaTime, SDL_Event event);

	Sphere* GetBoundingSphere()const { return _collisionSphere; };

	void HitByBall();
	bool CheckDeath();

	int GetBallHitCount() const { return _ballHitCount; };

private:
	Vector3 _position;
	Vector3 _scale;
	float _moveSpeed;
	float _rotAngle = 0.0f;

	string _name;

	char _fileName[30];
	char _textureName[30];
	Texture2D* _texture;

	Sphere* _collisionSphere;

	objType _object;

	Mix_Chunk* _scream;
	int _screamVolume;
	
	int _lastKeyPressed = 0;

	int _ballHitCount = 0;


};

#endif //_OBJECT3DS_H_