#ifndef _CAMERA_H
#define _CAMERA_H

#include "Commons.h"
#include <SDL.h>
#include "Vector3.h"

class Camera
{
public:
	Camera();
	~Camera();

	static Camera* GetInstance();
	void Update(float deltaTime, SDL_Event e);
	void Render();

private:
	Vector3 _position = Vector3(0.0, -1.3, 1.3);
	Vector3 _forward = Vector3();
	Vector3 _up = Vector3();
	Vector3 _right = Vector3();

	//horizontal angle : toward -z.   Rotation around y-axis
	float _horizontalAngle = 3.14f;
	//vertical angle : 0, look at horizon.   Rotaion around x-axis
	float _verticalAngle = 1.0f;
};

#endif //_CAMERA_H
