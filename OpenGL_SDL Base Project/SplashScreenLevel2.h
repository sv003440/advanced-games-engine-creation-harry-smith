#pragma once
#include "GameScreen.h"
#include "HUD.h"
#include "Camera.h"

class GameScreenManager;

class SplashScreenLevel2 : public GameScreen
{
public:
	SplashScreenLevel2(GameScreenManager* GSManager);
	~SplashScreenLevel2();

	void Render() override;
	void Update(float deltaTime, SDL_Event e) override;

private:
	GameScreenManager* _GSManager;
	Camera* _camera;
	HUD* _message;

};