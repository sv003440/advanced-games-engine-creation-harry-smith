#include "Ball.h"

Ball::Ball(Vector3 position, double radius, Vector3 startingVelocity, Vector3 gravityForce, float mass) : _position(position), _radius(radius), _velocity(startingVelocity), _gravity(gravityForce), _mass(mass), _hitDenzil(false)
{
	_bounceSound = Mix_LoadWAV("Sounds\\Bounce.wav");
	//rather than loading in for each ball this should be passed in
	_boundingSphere = new Sphere(position, radius, _name);
	_groundHeight = radius;
	_denzilSphere = nullptr;
}

Ball::Ball(double radius, Vector3 gravityForce, float mass, string name) : _radius(radius), _gravity(gravityForce), _mass(mass), _hitDenzil(false), _name(name)
{
	//rather than loading in for each ball this should be passed in
	_bounceSound = Mix_LoadWAV("Sounds\\Bounce.wav");
	_boundingSphere = new Sphere(_position, radius, "ball");
	_groundHeight = radius;
	_denzilSphere = nullptr;
	OutOfBounds();
}

Ball::~Ball()
{
	Mix_FreeChunk(_bounceSound);
}

void Ball::Update(float deltaTime)
{
	if (!_hitDenzil)
	{
		if (!HasHitGround())
		{
			ApplyForce(_gravity);
		}
		
		CheckOutOfBounds();

		UpdateAcceleration();

		CheckCollision();

		_position += ((_velocity + (_acceleration * 0.5f * deltaTime)) * deltaTime);

		_velocity += _acceleration * deltaTime;

		_boundingSphere->Update(_position, _velocity);
		_boundingSphere->SetCollided(false);

		_totalForce = Vector3();
	}
	else
	{
		_position = _denzilSphere->GetCentre();
	}
}

void Ball::Render()
{
	glPushMatrix();

	glTranslatef(_position.x, _position.y, _position.z);
	glColor3f(0.0, 0.0, 0.0);
	glutSolidSphere(_radius, 10, 10);
//	glutWireSphere(_radius, 10, 10);

	
	glPopMatrix();
}

void Ball::UpdateAcceleration()
{
	if (_mass == 0)
		return;
	_acceleration = _totalForce / _mass;
}

void Ball::OutOfBounds()
{
	random_device rando;//<--I don't really know what this is for but it works :(
	uniform_real_distribution<float> dist(-1, 1);

	mt19937 mersenne(rando());

	float x = dist(mersenne);
	float y = dist(mersenne) / 2;
	_position = Vector3(x, y, 1.0f);

	float i = dist(mersenne);
	float j = dist(mersenne);
	float k = -1 * abs(dist(mersenne));

	_velocity = Vector3(i, j, k);
}

void Ball::CheckCollision()
{
	Vector3 prevPos = _position;

	if (_boundingSphere->GetCollided())
	{
		if (_boundingSphere->GetCollidedWith() == "Denzil")
		{
			_hitDenzil = true;
			_denzilSphere = _boundingSphere->getOtherSphere();
			return;

		}
	}
}

bool Ball::HasHitGround()
{
	if (_position.z < _groundHeight)
	{
		Mix_PlayChannel(-1, _bounceSound, 0);

		_position.z = _groundHeight;

		float floorMass = 100.0f;
		float coefRestFloor = 0.9f;
		Vector3 floorVel = Vector3();

		float prevVel_X = _velocity.x;
		float prevVel_Y = _velocity.y;
		_velocity = (
			(_velocity * _mass)
			+ (
			(floorVel - _velocity)
				* (floorMass * coefRestFloor)
				)
			)
			/ (floorMass + _mass);
		_velocity.x = prevVel_X * coefRestFloor;//a bit hacky but it stops the ball from bouncing back on itself
		_velocity.y = prevVel_Y * coefRestFloor;
		return true;
	}
	return false;
}

void Ball::CheckOutOfBounds()
{
	if (abs(_position.x) > 1.0f || abs(_position.y) > 0.5f)
	{
		OutOfBounds();
	}
}